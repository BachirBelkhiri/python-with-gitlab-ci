from func import add, substract, multiply, divide
from os import getcwd, mkdir, path


if __name__ == "__main__":
    print("Add: ", add(10, 5))
    print("Substract: ", substract(10, 5))
    print("Multiply: ", multiply(10, 5))
    print("Divide:", divide(10, 5))
    dir = "artifact"
    path = path.join(getcwd(), "artifact")
    mkdir(path)
    open(f"{dir}/app.war", "wb").close()
