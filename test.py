import unittest
import func


class TestFunc(unittest.TestCase):

    def test_add(self):
        self.assertEqual(func.add(10, 5), 15)
        self.assertEqual(func.add(-1, 1), 0)
        self.assertEqual(func.add(-1, -1), -2)

    def test_substract(self):
        self.assertEqual(func.substract(10, 5), 5)
        self.assertEqual(func.substract(-1, 1), -2)
        self.assertEqual(func.substract(-1, -1), 0)

    def test_multiply(self):
        self.assertEqual(func.multiply(10, 5), 50)
        self.assertEqual(func.multiply(-1, 1), -1)
        self.assertEqual(func.multiply(-1, -1), 1)

    def test_divide(self):
        self.assertEqual(func.divide(10, 5), 2)
        self.assertEqual(func.divide(-1, 1), -1)
        self.assertEqual(func.divide(-1, -1), 1)


if __name__ == '__main__':
    unittest.main()
